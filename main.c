#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_NOM 15
#define MAX_TIPUS 35
#define NOM_FITXER "animals.dat"
#define BB while(getchar()!='\n')
#define ERROR_OBRIR_FITXER "Error en obrir el fitxer"
#define ERROR_ESCRIPTURA_FITXER "Error d'escriptura al fitxer"
#define ERROR_LECTURA_FITXER "Error de lectura al fitxer"
#define ERROR_OPERACIO_CANCELADA "Operacio Cancelada"
#define ERROR_NO_BORRAR "No s'ha pogut borrar el fitxer"
#define ERROR_RENOM_FITXER "No ha pogut renombrar el fitxer"
#define INFO_NO_ERROR "operacions realitzades amb exit"
typedef struct{
	char tipus[MAX_TIPUS+1];
	char nom[MAX_NOM+1];
	char sexe;// m-masculi, f-femeni
	bool enPerillExtincio;
	int edat;
	double pes;
	char marcaEsborrat;
} Animal; //estructura que representa el fitxer intern.

void escriureAnimal(Animal animal);
void entrarAnimal(Animal *animal,bool modifica);
void entraOpcio(int *opcio);
void pintarMenu();
int alta(char nomFitxer[]);
int consulta(char nomFitxer[],bool esborrats);
int baixaModifica(char NomFitxer[], bool esModifica);
bool seguirBaixaModifica(bool esModifica);
int esborrarFitxer(char NomFitxer[]);
int compactarFitxer(char nomFitxer[]);
int esborrarFitxerFinal(char nomFitxer[]);
int numeroRegistres(char nomFitxer[], long *registre);
int accesDirecte(char nomFitxer[]);

int main()
{
    int opcio, error;
    long registre;
    do{
        pintarMenu();
        entraOpcio(&opcio);
        switch(opcio){
        case 1:
                error=alta(NOM_FITXER);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-2) printf(ERROR_ESCRIPTURA_FITXER);
                break;
        case 2:
                error = baixaModifica(NOM_FITXER, false);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -2)printf(ERROR_ESCRIPTURA_FITXER);
                if(error == -3)printf(ERROR_LECTURA_FITXER);
                if(error == -4)printf(ERROR_OPERACIO_CANCELADA);
                break;
        case 3:
                error=consulta(NOM_FITXER,false);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-2) printf(ERROR_ESCRIPTURA_FITXER);
                if(error==-3) printf(ERROR_LECTURA_FITXER);
                break;
        case 4:
                error = baixaModifica(NOM_FITXER, true);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -2)printf(ERROR_ESCRIPTURA_FITXER);
                if(error == -3)printf(ERROR_LECTURA_FITXER);
                if(error == -4)printf(ERROR_OPERACIO_CANCELADA);
                break;
            case 5:
                error = consulta(NOM_FITXER, true);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -3)printf(ERROR_LECTURA_FITXER);
                break;
            case 6:
                error = esborrarFitxerFinal(NOM_FITXER);
                printf("\n");
                if(error == 0){printf(INFO_NO_ERROR); getchar();}
                if(error == -4)printf(ERROR_OPERACIO_CANCELADA);
                if(error == -6)printf(ERROR_NO_BORRAR);
                break;
            case 7:
                error = compactarFitxer(NOM_FITXER);
                printf("\n");
                if(error == 0){printf(INFO_NO_ERROR); getchar();}
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -3)printf(ERROR_LECTURA_FITXER);
                if(error == -5)printf(ERROR_RENOM_FITXER);
                break;
            case 8:
                error = numeroRegistres(NOM_FITXER,&registre);
                printf("Hi ha %ld registres actualment", registre);
                printf("\n""enter per continuar""\n");
                getchar();
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -3)printf(ERROR_LECTURA_FITXER);
                break;
            case 9:
                error = accesDirecte(NOM_FITXER);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                break;
            case 10:
                error = informe(NOM_FITXER);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -2)printf(ERROR_ESCRIPTURA_FITXER);
                if(error == -3)printf(ERROR_LECTURA_FITXER);
                break;
            case 0:
                break;
            default:
                printf("\n numeros valids entre 1 i 7 \n");
        }
        if(error != 0){printf("\nPrem una tecla per continuar...\n"); getchar();}
    }while(opcio!=0);

    return 0;
}



void entraOpcio(int *opcio){
    scanf("%d",opcio);BB;
}

void pintarMenu(){
    system("cls || clear");
    printf("**** MENU *****\n");
    printf("***************\n\n");
    printf("\t1 - Alta\n");
    printf("\t2 - Baixa\n");
    printf("\t3 - Consulta\n");
    printf("\t4 - Modificar\n");
    printf("\t5 - Consulta Esborrats\n");
    printf("\t6 - Esborrar fitxer\n");
    printf("\t7 - Compactar fitxer\n");
    printf("\t8 - Numero de registres\n");
    printf("\t9 - Acces directe\n");
    printf("\t10 - Informe\n");
    printf("\t0 - Sortir\n");
    printf("Tria opció: (0-7)");
}


int alta(char nomFitxer[]){
    system("cls || clear");
    Animal a1;
    FILE *f1;
    int n;
    f1= fopen(nomFitxer,"ab");
    if( f1 == NULL ) {
        return -1;
    }
    entrarAnimal(&a1,false);
    n= fwrite(&a1,sizeof(Animal),1,f1);
    if(n==0) {
        return -2;
    }
    fclose(f1);
    return 0;
}

int consulta(char nomFitxer[],bool esborrats){
    Animal a1;
    FILE *f1;
    int n;
    system("clear");
    f1= fopen(nomFitxer,"rb");
    if( f1 == NULL ) {
        return -1;
    }
    system("clear");
    while(!feof(f1)){
        n = fread(&a1,sizeof(Animal), 1,f1);
        if(!feof(f1)){
            if(n==0) {
                return -3;
            }
            if(!esborrats){
                if(a1.marcaEsborrat != '*')escriureAnimal(a1);
            }else{
                if(a1.marcaEsborrat == '*')escriureAnimal(a1);
            }

        }
    }
    fclose(f1);
    printf("\nPerm una tecla per continuar..."); getchar();
    return 0;
}

int baixaModifica(char NomFitxer[], bool esModifica){
    system("cls || clear");
    Animal a1;
    FILE *f1;
    char nomTmp[MAX_NOM];
    int n = 0;
    f1 = fopen(NomFitxer,"rb+");
    if(f1 == NULL){
        return -1;
    }
    if(esModifica == false){
        printf("\nIntrodueix el nom de l'animal per donar de baixa: ");
    }else{printf("\nIntrodueix el nom de l'animal per modificar: ");}
    scanf("%15[^\n]",nomTmp);BB;
    while(!feof(f1)){
        n = fread(&a1,sizeof(Animal),1,f1);
        if(!feof(f1)){
            if(n == 0){
                return -3;
            }
            if(a1.marcaEsborrat != '*' && strcmp(a1.nom,nomTmp) == 0){
                escriureAnimal(a1);
                if(!seguirBaixaModifica(esModifica)) {
                        return -4;
                }
                else{
                    if(fseek(f1, -(long) sizeof(Animal), SEEK_CUR)){
                            return -2;
                    }
                    if(esModifica == false){
                        a1.marcaEsborrat = '*';
                    }else{entrarAnimal(&a1, true);}
                    n = fwrite(&a1, sizeof(Animal), 1, f1);
                    if(n == 0) {
                        return -3;
                    }
                    break;
                }
            }
        }
    }
    fclose(f1);
    printf("\nPrem una tecla per continuar...\n");
    return 0;
}

bool seguirBaixaModifica(bool esModifica){
    char opcio;
    bool segur;
    do{
        if(esModifica == false){
            printf("\nvols donar de Baixa?(s/n): ");
        }else{printf("\nvols Modificar?(s/n): ");}
        scanf("%c",&opcio);BB;
    }while(opcio != 's' && opcio != 'n');
    segur = (opcio == 's')?true:false;
    return segur;
}


int esborrarFitxer(char NomFitxer[]){
    return unlink(NomFitxer);
}


int compactarFitxer(char nomFitxer[]){
    Animal a1;
    int n;
    FILE *origen, *desti;
    origen = fopen(nomFitxer,"rb");
    if(origen == NULL){
        return -1;
    }
    desti = fopen("tmp.dat","wb");
    if(desti == NULL){
        return -1;
    }

    while(!feof(origen)){
        n = fread(&a1,sizeof(Animal),1,origen);
        if(!feof(origen)){
            if(n == 0){
                return -3;
            }
            if(a1.marcaEsborrat != '*'){
                n = fwrite(&a1, sizeof(Animal),1,desti);
            }
        }
    }
    fclose(origen);
    fclose(desti);
    esborrarFitxer(nomFitxer);
    if(rename("tmp.dat",NOM_FITXER) == -1){
        return -5;
    }
    return 0;
}

int esborrarFitxerFinal(char nomFitxer[]){
    char opcio;
    int n = 0;
    do{
        printf("\nSegur que vols eliminar permanent el fitxer? (s/n): ");
        scanf("%c",&opcio);BB;
    }while(opcio != 's' && opcio != 'n');
    if(opcio == 's'){
        n = esborrarFitxer(nomFitxer);
        if(n!=0){
            n = -6;
        }
    }else n = -4;
    return n;
}

int numeroRegistres(char nomFitxer[], long *registre){
    system("cls || clear");
    Animal a1;
    int n;
    long nReg = 0;
    FILE *f1;
    f1=fopen(nomFitxer,"rb");
    if(f1==NULL) return -1;
    while(!feof(f1)){
        n=fread(&a1,sizeof(Animal),1,f1);
        if(!feof(f1)){
            if(n==0){
                return -3;
            }
        }
    }
    fseek(f1,0L,SEEK_END);
    nReg=ftell(f1);
    nReg=nReg/sizeof(Animal);
    fclose(f1);
    *registre=nReg;
    return 0;
}

int accesDirecte(char nomFitxer[]){
    system("cls || clear");
    Animal a1;
    int n;
    int pos;
    long total;
    int error;
    error = numeroRegistres(NOM_FITXER,&total);
    if(error!=0) {
        return -1;
    }
    if(total==0) {
        return -7;
    }
    do{
    printf("Quin registre vols accedir? MAX (%ld): ",total);
    scanf("%d",&pos);BB;
    }while(pos<=0||pos>total);
    FILE *f1;
    f1 = fopen(nomFitxer,"rb");
    if(f1==NULL){
        return -1;
    }
    n=pos-1;
    fseek(f1,(long)((n)*sizeof(Animal)),SEEK_SET);
    fread(&a1,sizeof(Animal),1,f1);
    if(a1.marcaEsborrat != '*'){
        escriureAnimal(a1);
    }else printf("\nRegistre no existeix o esta esborrat\n");
    fclose(f1);
    printf("\n""clica enter per continuar..""\n");
    getchar();
    return 0;
}

int informe(char nomFitxer[]){
    system("cls || clear");
    char html[]="<!DOCTYPE html> \n\
<html lang='en'> \n\
\t<head>\n\
  \t\t<meta charset='UTF-8'>\n\
  \t\t<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n\
  \t\t<title>Document</title>\n\
\t</head>\n\
\t<body>\n\
<table>\
<tr>Nom</tr>\
<tr>tipus</tr>\
<tr>edat</tr>\
<tr>pes</tr>\
<tr>perill</tr>\
<tr>sexe</tr>";
//Escriptura de les dades la taula
char finahtml[]="</table>\
                \t</body>\n\
                </html>\n";

    FILE *fitxerInformeHTML;
    FILE *fitxerAnimals;
    Animal animal;
    int n;
    char total[500];
    char cadenaTmp[50];


    fitxerInformeHTML=fopen("informe.html","w");
    if(fitxerInformeHTML==NULL) {
        return -1;
    }
    fitxerAnimals=fopen("animals.dat","r");
    if(fopen("animals.dat","r")==NULL){
        return -1;
    }
    while(!feof(fitxerAnimals)){
        n = fread(&animal,sizeof(Animal),1,fitxerAnimals);
        if(!feof(fitxerAnimals)){
            if(n == 0){
                return -3;
            }
            if(animal.marcaEsborrat!='*'){
                total[0]='\0';
                strcat(total, "\n\t\t\t<tr>");
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total,animal.nom);
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total, "\n\t\t\t\t<td>");
                sprintf(cadenaTmp,"%d",animal.edat);
                strcat(total,cadenaTmp);
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total, "\n\t\t\t\t<td>");
                sprintf(cadenaTmp,"%.2lf",animal.pes);
                strcat(total,cadenaTmp);
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total, "\n\t\t\t\t<td>");
                if(animal.enPerillExtincio)strcpy(cadenaTmp,"Si");
                else strcpy(cadenaTmp,"No");
                strcat(total,cadenaTmp);
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total, "\n\t\t\t\t<td>");
                if(animal.sexe=='m')strcpy(cadenaTmp,"Mascle");
                else strcpy(cadenaTmp,"Femella");
                strcat(total,cadenaTmp);
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total,animal.tipus);
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total, "\n\t\t\t<tr>");
            }
        }
    }
    fputs(finahtml,fitxerInformeHTML);
    fclose(fitxerAnimals);
    fclose(fitxerInformeHTML);
    system("firefox index.html");
    printf("\nInforme generat correctament");
    printf("\nPrem enter per coninuar");

    return 0;
}


void entrarAnimal(Animal *animal,bool modifica){
    system("cls || clear");
	char enPerill;
    printf("\nIntrodueix el nom: ");
    scanf("%15[^\n]",animal->nom);BB;
    printf("\nIntrodueix el tipus: ");
    scanf("%35[^\n]",animal->tipus);BB;
    do{
        printf("\nIntrodueix el sexe: ");
        scanf("%c",&animal->sexe);BB;
    }while(animal->sexe!='m' && animal->sexe!='f');
    printf("\nIntrodueix la edat: ");
    scanf("%d",&animal->edat);BB;
    printf("\nIntrodueix el pes: ");
    scanf("%lf",&animal->pes);BB;
    do{
        printf("\nEstà en perill d'extincio (s/n)? ");
        scanf("%c",&enPerill);BB;
    }while(enPerill!='s' && enPerill!='n');
    if(enPerill=='s') animal->enPerillExtincio=true;
    else animal->enPerillExtincio=false;
}


void escriureAnimal(Animal animal){
    printf("\nIntrodueix el nom: %s",animal.nom);
    printf("\nIntrodueix el tipus: %s",animal.tipus);
    printf("\nIntrodueix el sexe: %c",animal.sexe);
    printf("\nIntrodueix la edat: %d",animal.edat);
    printf("\nIntrodueix el pes: %lf",animal.pes);
    if(animal.enPerillExtincio==true) printf("\nEstà en perill\n") ;
    else printf("\nNo esta en perill d'extinció\n") ;
}
